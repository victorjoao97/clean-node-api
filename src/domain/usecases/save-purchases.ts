import { PurchaseModel } from '@/domain/models'

export interface ISavePurchases {
  save: (purchases: ISavePurchases.Params[]) => Promise<void>
}

export namespace ISavePurchases {
  export type Params = PurchaseModel
}