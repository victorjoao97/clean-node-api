import { CachePolicy, ICacheStore } from '@/data/protocols/cache'
import { ILoadPurchases, ISavePurchases } from '@/domain/usecases'

export class LocalLoadPurchases implements ISavePurchases, ILoadPurchases {
  private readonly key = 'purchases'
  constructor(
    private readonly cacheStore: ICacheStore,
    private readonly currentDate: Date
  ) {}

  async save(purchases: ISavePurchases.Params[]): Promise<void> {
    this.cacheStore.replace(this.key, {
      timestamp: this.currentDate,
      value: purchases
    })
    return
  }

  async loadAll (): Promise<ILoadPurchases.Result[]> {
    try {
      const cache = this.cacheStore.fetch(this.key)
      if (CachePolicy.validate(cache.timestamp, this.currentDate)) {
        return cache.value
      } else {
        throw new Error()
      }
    } catch {
      this.cacheStore.delete(this.key)
      return []
    }
  }
}
