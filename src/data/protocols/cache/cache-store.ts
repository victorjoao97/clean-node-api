export interface ICacheStore {
  fetch: (key: string) => any
  delete: (key: string) => void
  insert: (key: string, purchases: any) => void
  replace: (key: string, purchases: any) => void
}